import React, { Component } from 'react';

import { withFirebase } from '../firebase';


class FormsDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      form: null,
      ...props.location.state,
    };
  }

  componentDidMount() {
    if (this.state.form) {
      return;
    }

    this.setState({ loading: true });

    this.props.firebase
      .form(this.props.match.params.id)
      .on('value', snapshot => {
        this.setState({
          form: snapshot.val(),
          loading: false,
        });
      });
  }

  componentWillUnmount() {
    this.props.firebase.form(this.props.match.params.id).off();
  }

  
  render() {
    const { form, loading } = this.state;

    return (
      <div>
        <h2>Form ({this.props.match.params.id})</h2>
        {loading && <div>Loading ...</div>}

      
            <div className='col-sm-8'>
           <table class="table">
       <thead>
       <tr>
       <th scope="col"> ID </th>
       <th scope="col"> E-Mail </th>
       <th scope="col"> Username </th>
       <th scope="col">Update</th>
       </tr>
       </thead>
       <tbody>
       {form && (
       <tr>
       <td>{form.uid}</td>
       <td>{form.email}</td>
       <td>{form.username}</td>
       <td><button
                      
                     >
                       Send Password Reset
                     </button></td>
       
       </tr>
       )}
       </tbody>
       </table></div>
       }
       
      </div>
    );
  }
}

export default withFirebase(FormsDetails);
