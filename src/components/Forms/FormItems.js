import React, { Component } from 'react';
import firebaseConf from 'firebase'
import { withFirebase } from '../firebase';
import { Link } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';

class FormItems extends Component {
    
  constructor(props) {
    super(props);
    this.state = {
      loading : false,
      form : [],
 
    };
  }

  omponentDidMount() {
    this.setState({ loading: true });

    this.props.firebase.form().on('value', snapshot => {
      const formObject = snapshot.val();

      const formList = Object.keys(formObject).map(key => ({
        ...formObject[key],
        uid: key,
      }));

      this.setState({
        form: formList,
        loading: false,
      });
    });
  }

  showAlert(type, message) {
    this.setState({
      alert: true,
      alertData: { type, message }
    });
    setTimeout(() => {
      this.setState({ alert: false });
    }, 4000)
  }
  
  componentWillMount() {
    let formRef = firebaseConf.database().ref('form').orderByKey().limitToLast(16);
    formRef.on('child_added', snapshot => {
    const { nomS, adresseS, montant ,dateE , prixE , prixP , tauxI, dateR, duree, AssuranceC, denomination , cotation , droitA , restrictionV , dateAdmin , produitN } = snapshot.val();
    const data = { nomS, adresseS, montant ,dateE , prixE , prixP , tauxI, dateR, duree, AssuranceC, denomination , cotation , droitA , restrictionV , dateAdmin , produitN };
      this.setState({ form: [data].concat(this.state.form) });
    })
  }
  render() {
    return (<div className='col-sm-8'>
    <table class="table">
<thead>
<tr>
<th scope="col">Nom Societe</th>
<th scope="col">Montant</th>
<th scope="col">Date Emission</th>
<th scope="col">Prinx Emission</th>
<th scope="col">Prix P</th>
<th scope="col">Taux Interet</th>
<th scope="col">Date R</th>
<th scope="col">Details</th>
</tr>
</thead>
<tbody>
{this.state.form.map(form =>
<tr>

<td>{form.nomS}</td>
<td>{form.montant}</td>
<td>{form.dateE}</td>
<td>{form.prixE}</td>
<td>{form.prixP}</td>
<td>{form.tauxI}</td>
<td>{form.dateR}</td>
<td><Link
               to={{
               pathname: `${ROUTES.FORM2}/${form.uid}`,
               state: { form },
                }}
              > Details </Link></td>
     
     
</tr>
)}
</tbody>
</table></div>);}
}

export default withFirebase(FormItems);


