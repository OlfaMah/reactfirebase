import React from 'react';
import Applic from './Applic';
import {Provider} from 'react-redux';
import {store} from './chat'; 

const ChatbotPage = () => (
    <Provider store={store}>
    <Applic /> 
    </Provider>
);


export default ChatbotPage;
