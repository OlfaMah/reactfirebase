import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { FormItems, FormsDetails }  from '../Forms';
import * as ROUTES from '../../constants/routes';
import * as ROLES from '../../constants/roles';
import { withAuthorization } from '../Session';
import { compose } from 'recompose';

const Form2 = () => (
  <div>
    <h1>Formulaire</h1>
    <p>The Admin Page is accessible by every signed in admin user.</p>

    <Switch>
      <Route exact path={ROUTES.FORM2_DETAILS} component={FormsDetails} />
      <Route exact path={ROUTES.FORM2} component={FormItems} />
    </Switch>
  </div>
);

const condition = authUser =>
  authUser && !!authUser.roles[ROLES.ADMIN];

export default compose(
  withAuthorization(condition),
)(Form2);
