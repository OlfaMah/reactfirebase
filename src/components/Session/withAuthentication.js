import React from 'react';

import AuthUserContext from './context';
import { withFirebase } from '../firebase';
const withAuthentication = Component => {
  class WithAuthentication extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          authUser: null,
        };
      }
      componentDidMount() {
        //il utilise le programme d'écoute Firebase pour
        //déclencher une fonction de rappel chaque fois que l'utilisateur authentifié change
          this.listener = this.props.firebase.onAuthUserListener(
            authUser => {
              localStorage.setItem('authUser', JSON.stringify(authUser));
              this.setState({ authUser });
            },
            () => {
              localStorage.removeItem('authUser');
              this.setState({ authUser: null });
            },
          );
      }
      componentWillUnmount() {
        this.listener();
      }
    render() {
        return (
            <AuthUserContext.Provider value={this.state.authUser}>
              <Component {...this.props} />
            </AuthUserContext.Provider>
          );    }
  }
  return withFirebase(WithAuthentication);
};
export default withAuthentication;