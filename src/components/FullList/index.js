import React, { Component } from 'react';
import { compose } from 'recompose';
import { withFirebase } from '../firebase';
import { withAuthorization } from '../Session';
import firebaseConf from 'firebase';
import * as ROLES from '../../constants/roles';


const FullList = () => (
  <div>
    <h1>Welcome</h1>
    <p>Full Liste view</p>
   
    <Formulaires />
  </div>
);
class FormulaireBase extends Component {
    constructor(props) {
      super(props);
      this.state = {
        form: [],
        alert: false,
        alertData: {}
      };
    }
  
    showAlert(type, message) {
      this.setState({
        alert: true,
        alertData: { type, message }
      });
      setTimeout(() => {
        this.setState({ alert: false });
      }, 4000)
    }
  
    resetForm() {
      this.refs.contactForm.reset();
    }
  
    componentWillMount() {
      let formRef = firebaseConf.database().ref('form').orderByKey().limitToLast(16);
      formRef.on('child_added', snapshot => {
      const { nomS, rueS, numRS, codeP, localiteP, numIDE, tauxI, montant, prixE, dateE, dateR, nombreA, datePA, datePP, dateRA, emetteurC, butE, dateSE, item, montantC, valeurN, dateFinEC, fonctionA, prenomA, nomA, raisonS, dateI, dateAdmin, produitN, prixP, assuranceC, denomination, cotation, droitA, restrictionV } = snapshot.val();
      const data = { nomS, rueS, numRS, codeP, localiteP, numIDE, tauxI, montant, prixE, dateE, dateR, nombreA, datePA, datePP, dateRA, emetteurC, butE, dateSE, item, montantC, valeurN, dateFinEC, fonctionA, prenomA, nomA, raisonS, dateI, dateAdmin, produitN, prixP, assuranceC, denomination, cotation, droitA, restrictionV };
        this.setState({ form: [data].concat(this.state.form) });
      })
    }
  
    sendMessage(e) {
      e.preventDefault();
      /**nomS, adresseS, montant ,dateE , prixE , prixP , tauxI, dateR, AssuranceC, denomination , cotation , droitA , restrictionV , dateAdmin , produitN/ */
      const params = {
        nomS : this.inputNomS.value,
        rueS : this.inputRueS.value,
        numRS : this.inputNumRS.value,
        codeP : this.inputCodeP.value,
        localiteP : this.inputLocaliteP.value,
        numIDE : this.inputNumIDE.value,
        tauxI : this.inputTauxI.value,
        montant : this.inputMontant.value,
        prixE : this.inputPrixE.value,
        dateE : this.inputDateE.value,
        dateR : this.inputDateR.value,
        nombreA : this.inputNombreA.value,
        datePA : this.inputDatePA.value,
        datePP : this.inputDatePP.value,
        dateRA : this.inputDateRA.value,
        emetteurC : this.inputEmetteurC.value,
        butE : this.inputButE.value,
        dateSE : this.inputDateSE.value,
        item : this.inputItem.value,
        montantC : this.inputMontantC.value,
        valeurN : this.inputValeurN.value,
        dateFinEC : this.inputDateFinEC.value,
        fonctionA : this.inputFonctionA.value,
        nomA : this.inputNomA.value,
        prenomA : this.inputPrenomA.value,
        raisonS : this.inputRaisonS.value,
        dateI : this.inputDateI.value,
        dateAdmin : this.inputDateAdmin.value,
        produitN : this.inputProduitN.value,
        prixP : this.inputPrixP.value,
        assuranceC : this.inputAssuranceC.value,
        denomination : this.inputDenomination.value,
        cotation : this.inputCotation.value,
        droitA : this.inputDroitA.value,
        restrictionV : this.inputRestrictionV.value
        
      };
      if ( params.nomS && params.rueS && params.numRS && params.codeP && params.localiteP 
        && params.numIDE && params.tauxI && params.montant && params.prixE && params.dateE
        && params.dateR && params.nombreA && params.datePA && params.datePP && params.dateRA 
        && params.emetteurC && params.butE && params.dateSE && params.item && params.montantC
        && params.valeurN && params.dateFinEC && params.fonctionA && params.nomA && params.prenomA
        && params.raisonS && params.dateI && params.dateAdmin && params.produitN && params.prixP
        && params.assuranceC && params.denomination && params.cotation && params.droitA 
        ){
        firebaseConf.database().ref('form').push(params).then(() => {
          this.showAlert('success', 'Your message was sent successfull');
        }).catch(() => {
          this.showAlert('danger', 'Your message could not be sent');
        });
        this.resetForm();
      } else {
        this.showAlert('warning', 'Please fill the form');
      };
    }
  
    render() {
      return (
        <div>
          {this.state.alert && <div className={`alert alert-${this.state.alertData.type}`} role='alert'>
            <div className='container'>
              {this.state.alertData.message}
            </div>
          </div>}
          <div className='container' style={{ padding: `40px 40px` }}>
            <div className='row'>
              <div className='col-sm-4'>
                <h2>Informations relative aux obligations</h2>
                <form onSubmit={this.sendMessage.bind(this)} ref='contactForm' >
                  <div className='form-group'>
                    <label>Nom de la société</label>
                    <input type='text' className='form-control' id='nomS' placeholder='nom societe' ref={nomS => this.inputNomS = nomS}  required/>
                  </div>
                  <div className='form-group'>
                    <label>Adresse du siége</label>
                    <input type='text' className='form-control' id='rueS' placeholder='Rue Siége' ref={rueS => this.inputRueS = rueS} required/>
                    <input type='number' className='form-control' id='numRS' placeholder='num rue siege' ref={numRS => this.inputNumRS = numRS} required/>
                  </div>
                          <div className='form-group'>
                    <label>Code Postal</label>
                    <input type='text' className='form-control' id='codeP' placeholder='Code Postal' ref={codeP => this.inputCodeP = codeP} required/>
                  </div>
                          <div className='form-group'>
                    <label>Localité </label>
                    <input type='text' className='form-control' id='localiteP' placeholder='Localité ' ref={localiteP => this.inputLocaliteP = localiteP} required/>
                  </div>
                  <div className='form-group'>
                    <label>Numero IDE</label>
                    <input type="number" className='form-control' id='numIDE' placeholder='Numero IDE' ref={numIDE => this.inputNumIDE = numIDE} required />
                  </div>
                           <div className='form-group'>
                    <label>Taux D'intéret</label>
                    <input type="number" className='form-control' id='tauxI' placeholder='Taux Intéret' ref={tauxI => this.inputTauxI = tauxI} required/>
                  </div>
                  <div className='form-group'>
                    <label>Montant</label>
                    <input type="number" className='form-control' id='montant' placeholder='Montant' ref={montant => this.inputMontant = montant} required/>
                  </div>
                          <div className='form-group'>
                    <label>Prix d'émission</label>
                    <input type="number" className='form-control' id='prixE'  placeholder='Prix Emission' ref={prixE => this.inputPrixE = prixE} required />
                  </div>
                  <div className='form-group'>
                    <label>Date prévu d'émission</label>
                    <input type="date" className='form-control' id='dateE'  placeholder='Date Prévu Emission' ref={dateE => this.inputDateE = dateE} required />
                  </div>
                   <div className='form-group'>
                    <label>Date de remboursement</label>
                    <input type="date" className='form-control' id='dateR'  placeholder='Date de remboursement' ref={dateR => this.inputDateR = dateR} required />
                  </div>
                          <div className='form-group'>
                    <label>Nombre d'année </label>
                    <input type="number" className='form-control' id='nombreA' placeholder='Nombre Année(Durée)' ref={nombreA => this.inputNombreA = nombreA} required />
                  </div>
                  <div className='form-group'>
                    <label>Date Paiement Annuel</label>
                    <input type="date" className='form-control' id='datePA'  placeholder='Date Paiement Annuel' ref={datePA => this.inputDatePA = datePA} required />
                  </div>
                          <div className='form-group'>
                    <label>Date Premier Paiement</label>
                    <input type="date" className='form-control' id='datePP' placeholder='Date Premier Paiement' ref={datePP => this.inputDatePP = datePP} required />
                  </div>
                          <div className='form-group'>
                    <label>Date Rapport Audit</label>
                    <input type="date" className='form-control' id='dateRA' placeholder='Date Rapport Audit' ref={dateRA => this.inputDateRA = dateRA} required />
                  </div>
                  <div className='form-group'>
                    <label>Choisir L'emetteur</label>
                    <input type="text" className='form-control' id='emetteurC' placeholder='Choix Emetteur' ref={emetteurC => this.inputEmetteurC = emetteurC} required />
                  </div>
              
                        <div className='form-group'>
                    <label>Le but social de l'emetteur</label>
                    <input type="text" className='form-control' id='butE' placeholder='but social' ref={butE => this.inputButE = butE} required />
                  </div>
                  <div className='form-group'>
                    <label>Choisir la date du status de l'emetteur</label>
                    <input type="date" className='form-control' id='dateSE' placeholder='Choisir Date Status Emetteur' ref={dateSE => this.inputDateSE = dateSE} required />
                  </div>
                  <div className='form-group'>
                    <label>Choisir un Item</label>
                    <input type="text" className='form-control' id='item' placeholder='Choisir Item' ref={item => this.inputItem = item} required />
                  </div>
                  <div className='form-group'>
                    <label>Montant du Capital</label>
                    <input type="number" className='form-control' id='montantC' placeholder='Montant Capital' ref={montantC => this.inputMontantC = montantC} required />
                  </div>
                  <div className='form-group'>
                    <label>Valeur Nominale</label>
                    <input type="number" className='form-control' id='valeurN' placeholder='Valeur Nominale' ref={valeurN => this.inputValeurN = valeurN} required />
                  </div>
     
                  <div className='form-group'>
                    <label>Date de la fin de l'exercice comptable</label>
                    <input type="date" className='form-control' id='dateFinEC' placeholder='Valeur Nominale' ref={dateFinEC => this.inputDateFinEC = dateFinEC} required />
                  </div>
                  
                        <div className='form-group'>
                    <label>Conseil d'administration </label>
                           <label>Choisir la fonction </label>
                    <input type="text" className='form-control' id='fonctionA' placeholder='Fonction Administrateur' ref={fonctionA => this.inputFonctionA = fonctionA} required />
                     <label>Choisir son nom de famille </label>
                            <input type="text" className='form-control' id='nomA' placeholder='Nom Administrateur' ref={nomA => this.inputNomA = nomA} required />
                    <label>Choisir son prenom </label>
                            <input type="text" className='form-control' id='prenomA' placeholder='Prenom Administrateur' ref={prenomA => this.inputPrenomA = prenomA} required />
                        </div>
  
                 
                          <div className='form-group'>
                    <label>Choisir la raison social de l'organisation de révision</label>
                    <input type="text" className='form-control' id='raisonS' placeholder='Raison social' ref={raisonS => this.inputRaisonS = raisonS} required />
                  </div>
                  <div className='form-group'>
                    <label>Date de l'inscription au registre du commerce</label>
                    <input type="date" className='form-control' id='dateI' placeholder='Date inscription au registre du commerce' ref={dateI => this.inputDateI = dateI} required />
                  </div>
                   <div className='form-group'>
                    <label>Date de décision du conseil administrative</label>
                    <input type="date" className='form-control' id='dateAdmin' placeholder='Date de décision du conseil administrative'ref={dateAdmin => this.inputDateAdmin = dateAdmin} required />
                  </div>
                  <div className='form-group'>
                    <label>Utilisation du produit Net </label>
                    <input type="text" className='form-control' id='produitN'  placeholder='Utilisation du produit Net'  ref={produitN => this.inputProduitN = produitN} required />
                  </div>
                  <div className='form-group'>
                    <label>Prix de placement</label>
                    <input type="number" className='form-control' id='prixP' placeholder='Prix de placement' ref={prixP => this.inputPrixP = prixP} required />
                  </div>
                  
                  <div className='form-group'>
                    <label>Assurance Contractuelle</label>
                    <input type="text" className='form-control' id='AssuranceC' placeholder='Assurance Contractuelle' ref={AssuranceC => this.inputAssuranceC = AssuranceC} required />
                  </div>
                  <div className='form-group'>
                    <label>Dénomination</label>
                    <input type="number" className='form-control' id='denomination' placeholder='Dénomination'  ref={denomination => this.inputDenomination = denomination} required />(CHF)
                  </div>
                  <div className='form-group'>
                    <label>Cotation</label>
                    <input type="text" className='form-control' id='cotation' placeholder='Cotation' ref={cotation => this.inputCotation = cotation} required />
                  </div>
                  <div className='form-group'>
                    <label>Droit Applicable</label>
                    <input type="text" className='form-control' id='droitA' placeholder='Droit Applicable' ref={droitA => this.inputDroitA = droitA} required />
                  </div>
                  <div className='form-group'>
                    <label>Restriction de vente</label>
                    <input type="text" className='form-control' id='restrictionV' placeholder='Restriction de vente'  ref={restrictionV => this.inputRestrictionV = restrictionV} required />
                  </div>
  
                  <button type='submit' className='btn btn-primary'>Send</button>
                </form>
              </div>
               
                
              
            </div>
          </div>
         
        </div>
      );
    }
  }
  const Formulaires = withFirebase(FormulaireBase);
  const condition = authUser =>
    authUser && !!authUser.roles[ROLES.ADMIN];
  export default compose(
    withAuthorization(condition),
  )(FullList);