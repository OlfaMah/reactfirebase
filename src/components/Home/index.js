import React from 'react';
import Messages from '../Messages';



const HomePage = () => (
  <div>
    <h1>Welcome</h1>
    <Messages />

    <p>The Home Page is accessible by every signed in user.</p>
  </div>
);

export default HomePage;
