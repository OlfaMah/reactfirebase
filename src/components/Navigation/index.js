import React from 'react';
import { Link } from 'react-router-dom';
import SignOutButton from '../SignOut';
import * as ROUTES from '../../constants/routes';
import { AuthUserContext } from '../Session';
import * as ROLES from '../../constants/roles';

const Navigation = () => (
  <AuthUserContext.Consumer>
  {authUser =>
    authUser ? (
      <NavigationAuth authUser={authUser} />
    ) : (
      <NavigationNonAuth />
    )
  }
</AuthUserContext.Consumer>
);
const NavigationAuth = ({ authUser }) => (
  

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <Link  to={ROUTES.LANDING} class="navbar-brand">Application </Link>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <Link to={ROUTES.HOME} class="nav-item nav-link" >Home </Link>
      <Link  to={ROUTES.FULL} class="nav-item nav-link">View Full List</Link>
      <Link  to={ROUTES.ACCOUNT} class="nav-item nav-link">Account</Link>
      {!!authUser.roles[ROLES.ADMIN] && (
      <Link  to={ROUTES.ADMIN} class="nav-item nav-link">ADMIN</Link>
      )}
      <Link to={ROUTES.FORM2} class="nav-item nav-link">Form2</Link>
      <Link to={ROUTES.CHATBOT} class="nav-item nav-link">Chatbot</Link>
      <SignOutButton/> 

    </div>
  </div>
</nav>

);
const NavigationNonAuth = () => (

 <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <Link  to={ROUTES.LANDING} class="navbar-brand">Application </Link>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <Link to={ROUTES.LANDING} class="nav-item nav-link active">Landing</Link>
      <Link  to={ROUTES.SIGN_IN} class="nav-item nav-link">Sign In</Link>

    </div>
  </div>
</nav>
);

export default Navigation;